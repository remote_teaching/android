package ru.hnau.remote_teaching_android

import android.app.Application
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutType
import ru.hnau.androidutils.ui.utils.logD
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.remote_teaching_android.utils.managers.fcm.FCMManager


class App : Application() {

    override fun onCreate() {
        super.onCreate()

        //TODO remove after fix
        logD(LayoutType.Stretched.toString())
        logD(LayoutType.DEFAULT.toString())

        ContextConnector.init(this)
        FCMManager.sendPushTokenIfNeed()
    }

}