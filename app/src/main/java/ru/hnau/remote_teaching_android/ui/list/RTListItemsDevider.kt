package ru.hnau.remote_teaching_android.ui.list

import android.content.Context
import ru.hnau.androidutils.context_getters.dp_px.dp1
import ru.hnau.androidutils.ui.view.list.base.BaseListItemsDivider
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


object RTListItemsDevider {

    fun create(
        context: Context
    ) = BaseListItemsDivider(
        context = context,
        color = ColorManager.FG.mapWithAlpha(0.1f),
        size = dp1,
        paddingStart = SizeManager.DEFAULT_SEPARATION,
        paddingEnd = SizeManager.DEFAULT_SEPARATION
    )

}