package ru.hnau.remote_teaching_android.ui.cell

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.remote_teaching_android.ui.cell.line.LineCell
import ru.hnau.remote_teaching_android.ui.cell.line.LineCellColor
import ru.hnau.remote_teaching_android.utils.managers.ColorManager


@SuppressLint("ViewConstructor")
open class TitleCell<T : Any>(
    context: Context,
    dataGetter: (T) -> LineCell.Data<T>
) : LineCell<T>(
    context = context,
    activeColor = LineCellColor(
        titleColor = ColorManager.BG,
        subtitleColor = ColorManager.BG_T50
    ),
    inactiveColor = LineCellColor(
        titleColor = ColorManager.BG_T50,
        subtitleColor = ColorManager.BG_T50
    ),
    dataGetter = dataGetter,
    rippleDrawInfo = ColorManager.BG_ON_PRIMARY_RIPPLE_INFO
)