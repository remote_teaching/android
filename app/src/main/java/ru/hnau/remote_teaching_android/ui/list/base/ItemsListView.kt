package ru.hnau.remote_teaching_android.ui.list.base

import android.content.Context
import ru.hnau.androidutils.context_getters.dp_px.dp1
import ru.hnau.androidutils.ui.view.list.base.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.list.RTListItemsDevider
import ru.hnau.remote_teaching_android.utils.extensions.setBottomPaddingForMainActionButtonDecoration
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


class ItemsListView<T : Any>(
    context: Context,
    itemsProducer: Producer<List<T>>,
    viewWrappersCreator: (itemType: Int) -> BaseListViewWrapper<T>,
    idGetter: (T) -> Any
) : BaseList<T>(
    context = context,
    itemsProducer = itemsProducer,
    viewWrappersCreator = viewWrappersCreator,
    orientation = BaseListOrientation.VERTICAL,
    fixedSize = true,
    calculateDiffInfo = BaseListCalculateDiffInfo<T>(
        itemsComparator = { item1, item2 -> idGetter.invoke(item1) == idGetter.invoke(item2) },
        itemsContentComparator = { item1, item2 -> item1 == item2 }
    ),
    itemsDecoration = RTListItemsDevider.create(context)
) {

    init {
        setBottomPaddingForMainActionButtonDecoration()
    }

}