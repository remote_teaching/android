package ru.hnau.remote_teaching_android.ui.cell.line

import ru.hnau.androidutils.context_getters.ColorGetter


data class LineCellColor(
    val titleColor: ColorGetter,
    val subtitleColor: ColorGetter
)