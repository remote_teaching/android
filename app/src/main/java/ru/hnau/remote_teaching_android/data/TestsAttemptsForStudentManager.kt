package ru.hnau.remote_teaching_android.data

import ru.hnau.jutils.cache.AutoCache
import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_android.data.entity.LocalTestAttempt
import ru.hnau.remote_teaching_android.utils.extensions.sortKey


object TestsAttemptsForStudentManager : RTDataManager<List<LocalTestAttempt>>() {

    override suspend fun getValue() = API
        .getTestAttempts()
        .await()
        .map(::LocalTestAttempt)
        .sortedBy { attempt -> attempt.sortKey }

}