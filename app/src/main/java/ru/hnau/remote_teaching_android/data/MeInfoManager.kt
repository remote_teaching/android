package ru.hnau.remote_teaching_android.data


import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_common.data.User


object MeInfoManager : RTDataManager<User>(
    invalidateAfterUserLogin = true,
    valueLifetime = null
) {

    override suspend fun getValue() =
        API.me().await()

}