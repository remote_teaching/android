package ru.hnau.remote_teaching_android.utils

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.coroutines.UIJob
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.jutils.ifFalse
import ru.hnau.jutils.possible.Possible
import ru.hnau.jutils.tryCatch
import ru.hnau.jutils.tryOrNull
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.utils.extensions.text
import ru.hnau.remote_teaching_android.utils.managers.CrashliticsManager
import ru.hnau.remote_teaching_android.utils.managers.ErrorHandler
import ru.hnau.remote_teaching_common.exception.ApiException
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.coroutineContext

object Utils {

    fun copyTextToClipboard(label: String, text: String) {
        (ContextConnector.context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager)
            .primaryClip = ClipData.newPlainText(label, text)
    }

}

inline fun <T : Any> tryOrHandleError(action: () -> T) =
    tryCatch(
        throwsAction = { action.invoke() },
        onThrow = ErrorHandler::handle
    )

fun <T : Any> tryOrLogToCrashlitics(action: () -> T) =
    tryCatch(
        throwsAction = { action.invoke() },
        onThrow = { CrashliticsManager.handle(it) }
    )

@Deprecated("Use from JUtils")
fun <T> Iterable<T>.minusAt(position: Int) =
    filterIndexed { index, _ -> index != position }

fun <T> Iterable<T>.minusFirst(predicate: (T) -> Boolean): List<T> {
    var dropped = false
    var result = ArrayList<T>(count())
    for (element in this) {
        if (dropped || !predicate(element)) {
            result.add(element)
        } else {
            dropped = true
        }
    }
    return result
}


