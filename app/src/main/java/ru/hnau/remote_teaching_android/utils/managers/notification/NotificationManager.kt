package ru.hnau.remote_teaching_android.utils.managers.notification

import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.androidutils.utils.generateId
import ru.hnau.remote_teaching_android.AppActivity
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.utils.managers.notification.channels.NotificationChannelType
import ru.hnau.remote_teaching_android.utils.managers.notification.channels.NotificationChannelsManager


object NotificationManager {

    private const val DEFAULT_ID = 0

    private val notificationManager =
        NotificationManagerCompat.from(ContextConnector.context)

    private val DEFAULT_ON_CLICK: PendingIntent
        get() = PendingIntent.getActivity(
            ContextConnector.context,
            generateId(),
            Intent(ContextConnector.context, AppActivity::class.java),
            PendingIntent.FLAG_UPDATE_CURRENT
        )

    fun show(
        text: StringGetter,
        id: Int = DEFAULT_ID,
        channelType: NotificationChannelType = NotificationChannelType.DEFAULT,
        title: StringGetter = StringGetter(R.string.app_name),
        iconResId: Int = R.drawable.ic_notification,
        onClick: PendingIntent = DEFAULT_ON_CLICK,
        configurator: NotificationCompat.Builder.() -> Unit = {}
    ) {
        val context = ContextConnector.context
        val notification = NotificationCompat.Builder(
            context, channelType.id
        ).apply {
            setContentTitle(title.get(context))
            setContentText(text.get(context))
            setSmallIcon(iconResId)
            setContentIntent(onClick)
            setAutoCancel(true)
            configurator.invoke(this)
        }.build()
        show(id, notification)
    }

    private fun show(
        id: Int,
        notification: Notification
    ) {
        NotificationChannelsManager.initializeIfNeed()
        notificationManager.notify(id, notification)
    }

}