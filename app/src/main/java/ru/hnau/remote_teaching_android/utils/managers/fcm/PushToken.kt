package ru.hnau.remote_teaching_android.utils.managers.fcm

import ru.hnau.remote_teaching_android.utils.managers.SettingsManager


object PushToken {

    fun createKey(pushToken: String) =
        SettingsManager.host.let { "${it.length}$it$pushToken" }

}