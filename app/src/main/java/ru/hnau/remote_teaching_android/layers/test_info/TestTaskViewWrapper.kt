package ru.hnau.remote_teaching_android.layers.test_info

import android.content.Context
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.entity.TestTaskWithId
import ru.hnau.remote_teaching_android.ui.cell.CellAdditionalActionButton
import ru.hnau.remote_teaching_android.ui.cell.properties.PropertiesCell
import ru.hnau.remote_teaching_android.ui.cell.properties.PropertiesCellPropertyView
import ru.hnau.remote_teaching_android.utils.extensions.maxScoreUiString
import ru.hnau.remote_teaching_android.utils.extensions.typeUiString
import ru.hnau.remote_teaching_android.utils.extensions.variantsCountUiString
import ru.hnau.remote_teaching_android.utils.managers.ColorManager


class TestTaskViewWrapper(
    context: Context,
    onClick: (TestTaskWithId) -> Unit,
    onMenuClick: (TestTaskWithId) -> Unit
) : PropertiesCell<TestTaskWithId>(
    context = context,
    onClick = onClick,
    activeColor = ColorManager.PRIMARY,
    inactiveColor = ColorManager.FG,
    rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO,
    dataGetter = { task ->
        Data(
            title = StringGetter(R.string.test_info_layer_task_item_title, task.numberUiString),
            additionalActionButtonInfo = CellAdditionalActionButton.Info(
                icon = DrawableGetter(R.drawable.ic_options_primary),
                onClick = { onMenuClick(task) }
            )
        )
    },
    properties = listOf(
        PropertiesCellPropertyView.Data(
            name = StringGetter(R.string.test_info_layer_task_item_property_type),
            valueExtractor = { (_, testTask) ->
                testTask.typeUiString
            }
        ),
        PropertiesCellPropertyView.Data(
            name = StringGetter(R.string.test_info_layer_task_item_property_variants_count),
            valueExtractor = { (_, testTask) ->
                testTask.variantsCountUiString
            }
        ),
        PropertiesCellPropertyView.Data(
            name = StringGetter(R.string.test_info_layer_task_item_property_max_score),
            valueExtractor = { (_, testTask) ->
                testTask.maxScoreUiString
            }
        )
    )
)