package ru.hnau.remote_teaching_android.layers.students_group_test_results


data class StudentsGroupTestResultsListItem(
    val studentIdentifier: String,
    val score: Float,
    val maxScore: Int,
    val passed: Boolean
)