package ru.hnau.remote_teaching_android.layers.students_group_test_results

import android.content.Context
import android.view.View
import ru.hnau.jutils.getter.base.GetterAsync
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.list.base.ItemCellListContaner
import ru.hnau.remote_teaching_android.ui.list.base.ItemsListContaner
import ru.hnau.remote_teaching_android.utils.extensions.getLineCellData
import ru.hnau.remote_teaching_common.data.User


class StudentsGroupTestResultsListContainer(
    context: Context,
    producer: Producer<GetterAsync<Unit, List<StudentsGroupTestResultsListItem>>>,
    onClick: (StudentsGroupTestResultsListItem) -> Unit,
    onEmptyListInfoViewGenerator: () -> View,
    invalidator: () -> Unit
) : ItemsListContaner<StudentsGroupTestResultsListItem>(
    context = context,
    idGetter = { it.studentIdentifier },
    producer = producer,
    onEmptyListInfoViewGenerator = onEmptyListInfoViewGenerator,
    invalidator = invalidator,
    viewWrappersCreator = {
        StudentsGroupTestResultsListItemViewWrapper(
            context = context,
            onClick = onClick
        )
    }
)