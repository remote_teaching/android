package ru.hnau.remote_teaching_android.layers.student_section_info.list.item_view_wrapper

import android.content.Context
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.apply.addChild
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.remote_teaching_android.layers.student_section_info.list.StudentSectionInfoLayerListItem
import ru.hnau.remote_teaching_android.ui.cell.Cell
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.FontManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


class StudentSectionInfoLayerListContentMDViewWrapper(
    context: Context
) : Cell<StudentSectionInfoLayerListItem>(
    context = context,
    rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO
) {

    private val contentMDLabel = Label(
        context = context,
        fontType = FontManager.DEFAULT,
        gravity = HGravity.START_CENTER_VERTICAL,
        textSize = SizeManager.TEXT_16,
        textColor = ColorManager.FG
    )

    init {
        setPadding(SizeManager.DEFAULT_SEPARATION)
        addChild(contentMDLabel)
    }

    override fun onContentReceived(content: StudentSectionInfoLayerListItem) {
        contentMDLabel.text = (content.value as String).toGetter()
    }

}