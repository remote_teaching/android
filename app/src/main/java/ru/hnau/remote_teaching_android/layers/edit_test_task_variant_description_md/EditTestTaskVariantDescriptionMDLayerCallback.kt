package ru.hnau.remote_teaching_android.layers.edit_test_task_variant_description_md


interface EditTestTaskVariantDescriptionMDLayerCallback {

    fun onVariantDescriptionMDChanged(number: Int, descriptionMD: String)

}