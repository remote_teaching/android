package ru.hnau.remote_teaching_android.layers.section_info.list.item_view_wrapper

import android.content.Context
import ru.hnau.remote_teaching_android.layers.section_info.list.SectionInfoLayerListCallback
import ru.hnau.remote_teaching_android.layers.section_info.list.SectionInfoLayerListItem
import ru.hnau.remote_teaching_android.layers.section_info.list.SectionInfoLayerListItemType
import ru.hnau.remote_teaching_android.ui.cell.TitleCell


class SectionInfoLayerListTitleViewWrapper(
    context: Context,
    sectionInfoLayerListCallback: SectionInfoLayerListCallback
) : TitleCell<SectionInfoLayerListItem>(
    context = context,
    dataGetter = {
        (it.value as SectionInfoLayerListItemType).cellData.get(sectionInfoLayerListCallback)
    }
)