package ru.hnau.remote_teaching_android.layers.student_section_info.list.item_view_wrapper

import android.content.Context
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.layers.section_info.list.SectionInfoLayerListCallback
import ru.hnau.remote_teaching_android.layers.section_info.list.SectionInfoLayerListItem
import ru.hnau.remote_teaching_android.layers.student_section_info.list.StudentSectionInfoLayerListItem
import ru.hnau.remote_teaching_android.ui.cell.CellAdditionalActionButton
import ru.hnau.remote_teaching_android.ui.cell.ItemCell
import ru.hnau.remote_teaching_android.ui.cell.line.LineCell
import ru.hnau.remote_teaching_common.data.section.SectionSkeleton


class StudentSectionInfoLayerListSubsectionViewWrapper(
    context: Context,
    onSubsectionClick: (SectionSkeleton) -> Unit
) : ItemCell<StudentSectionInfoLayerListItem>(
    context = context,
    onClick = { item ->
        val subsection = item.value as SectionSkeleton
        onSubsectionClick(subsection)
    },
    dataGetter = { item ->
        val subsection = item.value as SectionSkeleton
        LineCell.Data<StudentSectionInfoLayerListItem>(
            title = subsection.title.toGetter()
        )
    }
)